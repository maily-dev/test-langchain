// import { ChatOpenAI } from "langchain/chat_models/openai";
// import { HumanChatMessage, SystemChatMessage } from "langchain/schema";
// import { } from 'dotenv/config'
// import express from 'express';


// const app = express();

// app.get('/', async (req, res) => {
//   console.log(req.query.test)

//   const chat = new ChatOpenAI({ temperature: 0, openAIApiKey: process.env.OPENAI_API_KEY });
//   const responseB = await chat.call([
//     new SystemChatMessage(
//       "If a client greets you, you usually ask for their name, then ask for the name of the project they need to work on. And tell them to describe what the product needs to do. Afterward"
//     ),
//     new HumanChatMessage("Hello"),
//   ]);

//   console.log(responseB);
// });

// app.listen(3000, () => {
//   console.log('Server listening on port 3000');
// });

import { ConversationChain } from "langchain/chains";
import { ChatOpenAI } from "langchain/chat_models";
import { MotorheadMemory } from "langchain/memory";

const model = new ChatOpenAI({ openAIApiKey: process.env.OPENAI_API_KEY });
const memory = new MotorheadMemory({
  sessionId: "user-id",
  motorheadUrl: "localhost:8080",
});

await memory.init(); // loads previous state from Motörhead 🤘
const context = memory.context
  ? `
Here's previous context: ${memory.context}`
  : "";

const chatPrompt = ChatPromptTemplate.fromPromptMessages([
  SystemMessagePromptTemplate.fromTemplate(
    `The following is a friendly conversation between a human and an AI. The AI is talkative and provides lots of specific details from its context. If the AI does not know the answer to a question, it truthfully says it does not know.${context}`
  ),
  new MessagesPlaceholder("history"),
  HumanMessagePromptTemplate.fromTemplate("{input}"),
]);

const chain = new ConversationChain({
  memory,
  prompt: chatPrompt,
  llm: chat,
});

const res1 = await chain.call({ input: "Hi! I'm Jim." });
console.log({ res1 });
